<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('sections.index');
});
Route::get('how-we-works', function () {
    return view('sections.how-we-works');
});
Route::get('contacts', function () {
    return view('sections.contacts');
});
Route::get('services', function () {
    return view('sections.services');
});
Route::get('technology', function () {
    return view('sections.technology');
});
Route::post('/store-feedback', 'HomeController@storeFeedback');
Route::group(['prefix' => 'admin', 'middleware'=>'auth'], function() {
    Route::resource('/meta-tags', 'Admin\\MetaTagsController');
    Route::resource('/texts', 'Admin\\TextsController');
    Route::resource('/news', 'Admin\\NewsController');
    Route::get('/get-feedback', 'AdminController@storeFeedback');
    Route::get('/', function (){return 'admin';});
});
Auth::routes();

Route::get('/register', function () {
    abort(404);
});

Route::get('/home', 'HomeController@index');


