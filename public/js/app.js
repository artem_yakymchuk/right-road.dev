
/*    ------------------ MAPS ------------------    */

function initMap() {
    var uluru = {lat: 48.259247, lng: 25.954602};

    if(document.documentElement.clientWidth <= 768){
   	 	var center = {lat: 48.260047, lng: 25.954602};
    } else {
   	 	var center = {lat: 48.259047, lng: 25.954602};
    }

    var mapUA = new google.maps.Map(document.getElementById('map-ua'), {
        zoom: 17,
        center: center
    });

    var markerUA = new google.maps.Marker({
        position: uluru,
        icon: '/img/mark.png',
        map: mapUA,
    });
   





    var uluru = {lat: 52.251882, lng: 21.033729 };
                
    if(document.documentElement.clientWidth <= 768){
   	 	var center = {lat: 52.261682, lng: 21.033729 };
    } else {
   	 	var center = {lat: 52.251682, lng: 21.033729 };
    }

    var mapPL = new google.maps.Map(document.getElementById('map-pl'), {
        zoom: 17,
        center: center,
    });

    var markerPL = new google.maps.Marker({
        position: uluru,
        icon: '/img/mark.png',
        map: mapPL,
    });
};

/*--------------------------------------------------------------------------------------------------------------------*/


















setTimeout(function() {

function closest(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
}





/*    ------------------ NAV MENU ------------------    */

(function(){

	var navMenu = document.querySelector('.nav-menu');
	var itemsMenu = document.querySelectorAll('.nm-item');
	var itemsMenuSlideCtrl = document.querySelectorAll('.nav-menu .ddm-content slider-x-control');

	var pages = {
		'#how-we-work': { 
			'slideIdx': 1,
			'menuIdx': 1
		}, 
		'#services': {
			'slideIdx': 2,
			'menuIdx': 1
		}, 
		'#technology': {
			'slideIdx': 3,
			'menuIdx': 1
		},
		'#our-team': {
			'slideIdx': 4,
			'menuIdx': 2
		},
		'#blog': {
			'slideIdx': 5,
			'menuIdx': 2
		},
		'#ukrainian': {
			'slideIdx': 6,
			'menuIdx': 3
		},
		'#poland': {
			'slideIdx': 7,
			'menuIdx': 3
		}
	};

	function defActive(index, setDef, setOver) {
		var lineMenu = document.querySelector('.line-menu .lm-indicator'); 
		lineMenu.classList.remove('index-0', 'index-1', 'index-2', 'index-3');
		
		if(setDef){
			lineMenu.classList.remove('def-index-0', 'def-index-1', 'def-index-2', 'def-index-3');
			lineMenu.classList.add('def-index-' + index);
		} else if(setOver){
			lineMenu.classList.add('index-' + index);
		}
	};

	function getHash(){
		return document.location.hash;
	}

	function getIndexByHash(hash){
		return pages[hash] ? pages[hash].menuIdx : undefined;
	}

	function setHash(slideIdx) {

		Array.prototype.forEach.call(itemsMenuSlideCtrl, function(item, index){
			item.classList.remove('active');
		});

		Array.prototype.forEach.call(document.querySelectorAll('.nm-item-main'), function(item, index){
			item.classList.remove('active');
		});

		var itemMenu = document.querySelector('.nav-menu .ddm-content slider-x-control[data-params="' + slideIdx + '"]');
		if(itemMenu) itemMenu.classList.add('active');

		var nmItem = document.querySelector('.nav-menu .nm-item-main[data-params="' + slideIdx + '"]')
		if(nmItem) nmItem.classList.add('active');

		function getKeyByValue(object) {
		  	return Object.keys(object).find(k => object[k]['slideIdx'] === slideIdx);
		}

		var hash = getKeyByValue(pages) || '';
		document.location.hash = hash;
		
		var title = 'Right Road';
		if(hash != '') title += ' | ';
		title += hash.slice(1);
		document.title = title;
	};






	
	/*    ------------------ SLIDER SWITCH ------------------    */

	var header  = document.querySelector('header');
	var footer  = document.querySelector('footer');

	sliderXMain.onSetSlide = function(activeSlide){
		setHash(sliderXMain.slideIndex);
        defActive(getIndexByHash(getHash()), true);

		if(activeSlide.getAttribute('data-reverse-color') == ""){
			setTimeout(function(){
				document.body.classList.add('reverse-color');
				header.classList.add('reverse-color'); 	
				footer.classList.add('reverse-color'); 	
			}, 200)
			
		} else {
			setTimeout(function(){
				document.body.classList.remove('reverse-color');
				header.classList.remove('reverse-color'); 	
				footer.classList.remove('reverse-color'); 	
			}, 200)
		}
	};
 
    /*------------------------------------------------------------*/



	(function() {
		
		var index = pages[document.location.hash] ? pages[document.location.hash].slideIdx : 0;
		var menuIdx = pages[document.location.hash] ? pages[document.location.hash].menuIdx : 0;
			
		sliderXMain.setSlide(index);

	})();



	Array.prototype.forEach.call(itemsMenu, function(item, index){
		
		item.addEventListener('mouseenter', function() {
			var itemIndex = Array.prototype.indexOf.call(this.parentNode.children, this);
			defActive(itemIndex, false, true);
		});

		item.addEventListener('mouseleave', function(){
			var itemIndex = Array.prototype.indexOf.call(this.parentNode.children, this);
			defActive(itemIndex); 
		});

	});

	Array.prototype.forEach.call(itemsMenuSlideCtrl, function(item, index){
		item.addEventListener('click', function() {
			
			Array.prototype.forEach.call(itemsMenuSlideCtrl, function(elm, index){
				elm.classList.remove('active');
			});
			
			this.classList.add('active');



			Array.prototype.forEach.call(document.querySelectorAll('.nm-item-main'), function(elm, index){
				elm.classList.remove('active');
			});

			var nmItemMian = closest(item, function(el) { return el && el.classList && el.classList.contains('drop-down-menu')});
			if(nmItemMian) nmItemMian.querySelector('.nm-item-main').classList.add('active');

		});
	});

})();




(function(){
	setTimeout(function() {
		document.querySelector('slide-x-container').style.transition = 'left 0.5s';
		document.querySelector('slide-x-container').style.webkitTransition = 'left 0.5s';
		document.querySelector('slide-x-container').style.msTransition = 'left 0.5s';
		document.querySelector('slide-x-container').style.oTransition = 'left 0.5s';
		document.querySelector('slide-x-container').style.mozTransition = 'left 0.5s';
	}, 100);
})();

/*--------------------------------------------------------------------------------------------------------------------*/












/*    ------------------ POP UP BTN ------------------    */

function rotate(item, deg){
	var regex = /\d+/g;
	var string = item.style.transform;
	var matches = string.match(regex) ? -parseInt(string.match(regex)[0]) + deg : deg;
	item.style.transform = "rotate(" + matches + "deg)";
}

setTimeout(function(){
    document.querySelector('body').addEventListener('click', function(event){
        var arrayPopUpButtons = document.querySelectorAll('.pop-up-open');
        var e = event || window.event;
        Array.prototype.forEach.call(arrayPopUpButtons, function(item){
        	var curentPopUp = closest(e.target, function(el) { return el && el.classList && el.classList.contains('pop-up')});
        	var parentPopUp = closest(item, function(el) { return el && el.classList && el.classList.contains('pop-up')});
            if (e.target != item && curentPopUp != parentPopUp) {
                if(item.parentNode.classList.contains('open')) rotate(item, -135);
                item.parentNode.classList.remove('open');
				item.parentNode.parentNode.classList.remove('open');

				var mobPopUp = document.querySelector('.mob-pop-up');
				mobPopUp.classList.remove('open');
            }
        })
    })
}, 100);



(function(){

	/*    --------------- POP UP FOR CONTACTS ---------------    */

	var buttons = document.querySelectorAll('.pop-up-contact .pop-up-open');
	Array.prototype.forEach.call(buttons, function(item){
		item.addEventListener("mouseenter", function(){ 
			if(!item.parentNode.classList.contains('open')) rotate(this, -90);
		});
		item.addEventListener("click", function(){ 
			this.parentNode.classList.toggle('open');
			this.parentNode.parentNode.classList.toggle('open');
			if(!item.parentNode.classList.contains('open')) rotate(this, -135);
			if(item.parentNode.classList.contains('open')) rotate(this, 135);
		});
	});
   
    /*------------------------------------------------------------*/

})();



(function(){

	/*    --------------- POP UP FOR BLOG ---------------    */

	var buttons = document.querySelectorAll('.blog .pop-up .pop-up-open');
	Array.prototype.forEach.call(buttons, function(item){
		// item.addEventListener("mouseenter", function(){ 
		// 	if(!item.parentNode.classList.contains('open')) rotate(this, -90);
		// });
		item.addEventListener("click", function(){ 
			this.parentNode.classList.toggle('open');
			this.parentNode.parentNode.classList.toggle('open');
			if(!item.parentNode.classList.contains('open')) rotate(this, -135);
			if(item.parentNode.classList.contains('open')) rotate(this, 135);
		});
	});
   
    /*------------------------------------------------------------*/

})();




(function(){
	
	/*    ------------- MOB POP-UP FOR SERVICES -------------    */

	var hexagons = document.querySelectorAll('.row .hexagon-lg');
	Array.prototype.forEach.call(hexagons, function(item){
		var popUp = item.querySelector('.pop-up');
		var popUpBtn = item.querySelector('.pop-up-open');

		if(popUpBtn)
		item.addEventListener("click", function(e){ 
			
			var parentPopUp = closest(e.target, function(el) { return el && el.classList && el.classList.contains('pop-up')});

			if(!popUp.classList.contains('open') || e.target == popUpBtn || parentPopUp === null){
				
				popUp.classList.toggle('open');
				popUp.parentNode.classList.toggle('open');
				if(!popUp.classList.contains('open')) rotate(popUpBtn, -135);
				if(popUp.classList.contains('open')) rotate(popUpBtn, 135);

				var mobPopUp = document.querySelector('.mob-pop-up');
				var mobPopUpContent = document.querySelector('.m-pp-content');
				mobPopUpContent.innerHTML = "";
				mobPopUpContent.appendChild(popUp.cloneNode(true));
				mobPopUp.classList.toggle('open');

				mobPopUp.querySelector('.pop-up-open').addEventListener('click', function(){
					mobPopUp.classList.toggle('open');
					rotate(popUpBtn, -135);
	                popUp.classList.remove('open');
					popUp.classList.remove('open');
				})
			}
		});

		if(popUpBtn)
		item.addEventListener("mouseenter", function(){ 
			if(!popUp.classList.contains('open')) rotate(popUpBtn, -90);
		});
	});

    /*------------------------------------------------------------*/

})();




(function(){

	/*    --------------- MOB POP-UP FOR BLOG ---------------    */

	var hexagons = document.querySelectorAll('.blog .hexagon-lg');
	Array.prototype.forEach.call(hexagons, function(item){
		var popUp = item.querySelector('.pop-up');
		var popUpBtn = item.querySelector('.pop-up-open');

		if(popUpBtn)
		item.addEventListener("click", function(e){ 
			
			if(!popUp.classList.contains('open') || e.target == popUpBtn || closest(e.target, function(el) { return el && el.classList && el.classList.contains('pop-up')})  === null){
				
				popUp.classList.toggle('open');
				popUp.parentNode.classList.toggle('open');
				if(!popUp.classList.contains('open')) rotate(popUpBtn, -135);
				if(popUp.classList.contains('open')) rotate(popUpBtn, 135);

				var parent = closest(this, function(el) { return el && el.classList && el.classList.contains('mc-blog')});
				var mobPopUp = parent.querySelector('.mob-pop-up');
				
				var mobPopUpCContent = document.querySelector('.m-pp-c-content');
				mobPopUpCContent.innerHTML = "";
				mobPopUpCContent.appendChild(popUp.cloneNode(true));

				var text = mobPopUpCContent.querySelector('.pop-up-content').innerHTML;
				var mobPopUpCContentP = mobPopUpCContent.querySelector('.pop-up-content');
				
				mobPopUpCContentP.innerHTML = '';
				mobPopUpCContentP.appendChild(item.querySelector('.hexagon-label').cloneNode(true));
				mobPopUpCContentP.appendChild(item.querySelector('.date').cloneNode(true));
				theKid = document.createElement("div");
				theKid.innerHTML = text;
				mobPopUpCContentP.appendChild(theKid);

				rotate(mobPopUpCContent.querySelector('.pop-up-open'), 45)

				mobPopUp.classList.toggle('open');
				mobPopUp.querySelector('.pop-up-open').addEventListener('click', function(){
					mobPopUp.classList.toggle('open');
					rotate(popUpBtn, -90);
	                popUp.classList.remove('open');
					popUp.classList.remove('open');
				})
			}
		});

		if(popUpBtn)
		item.addEventListener("mouseenter", function(){ 
			if(!popUp.classList.contains('open')) rotate(popUpBtn, -90);
		});
	});

    /*------------------------------------------------------------*/

})();

/*--------------------------------------------------------------------------------------------------------------------*/












/*    ------------------ RESIZE TEXTAREA ------------------    */

document.querySelector('textarea.pc-input').addEventListener('keyup', function() {
	enteredText = this.value;
	numberOfLineBreaks = (enteredText.match(/\n/g)||[]).length;
	if(numberOfLineBreaks < 6) this.setAttribute('rows', numberOfLineBreaks+2);
});

/*--------------------------------------------------------------------------------------------------------------------*/











/*    ------------------ MOBILE MENU ------------------    */

function MobMenu(){

	var self = this;

	this.isOpen = false;

	this.toggleOpen = function(){
		if(document.documentElement.clientWidth > 768 ) return 0;
		
		self.isOpen = !self.isOpen;

		document.querySelector('.m-menu-btn').classList.toggle('open');
		document.querySelector('.nav-menu').classList.toggle('open');
		document.querySelector('.footer-icons').classList.toggle('open');
		document.querySelector('.m-line-menu').classList.toggle('open');
		document.querySelector('.logo').classList.toggle('open');
		
		Array.prototype.forEach.call(document.querySelectorAll('.mc-item'), function(item) {
			item.classList.toggle('open');
		})
	}

	return self;
}

var mobMenu = new MobMenu();




document.querySelector('.m-menu-btn').addEventListener('click', function() {
	mobMenu.toggleOpen();
});

Array.prototype.forEach.call(document.querySelectorAll('.nav-menu slider-x-control'), function(item){
	item.addEventListener('click', function() {
		mobMenu.toggleOpen();
	});
});

Array.prototype.forEach.call(document.querySelectorAll('slider-x-control[data-slide="nextSlide"]'), function(item){
	item.addEventListener('click', function() {
		if(mobMenu.isOpen) mobMenu.toggleOpen();
	});
});



(function(){

	/*    ------------------ SWIPE EVENTS ------------------    */

	function ignoreSwipe(direction){
		if(direction == 'left' && sliderXMain.slideIndex == sliderXMain.length-1) return true;
		if(direction == 'right' && sliderXMain.slideIndex == 0) return true;
		return false;
	};

	var hammertime = new Hammer(document.body);
	hammertime.on('swipeleft', function(ev) {
		if(!ignoreSwipe('left')) sliderXMain.nextSlide();
		if(mobMenu.isOpen) mobMenu.toggleOpen();
	});
	hammertime.on('swiperight', function(ev) {
		if(!ignoreSwipe('right')) sliderXMain.prevSlide();
		if(mobMenu.isOpen) mobMenu.toggleOpen();
	});

    /*------------------------------------------------------------*/

})();



(function(){

	/*    ---------------- HORIZONTAL SCROLL ----------------    */
	
	document.querySelector('body').addEventListener('mousewheel', function(e){ 
		if(e.deltaY > 0 && sliderXMain.slideIndex != sliderXMain.length-1 ){
			sliderXMain.nextSlide();
			if(mobMenu.isOpen) mobMenu.toggleOpen();
		} else if(e.deltaY < 0 && sliderXMain.slideIndex != 0) {
			sliderXMain.prevSlide();
			if(mobMenu.isOpen) mobMenu.toggleOpen();
		}
	});

    /*------------------------------------------------------------*/

})();


/*--------------------------------------------------------------------------------------------------------------------*/

}, 100);




