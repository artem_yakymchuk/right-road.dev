var SliderPrototype = Object.create(HTMLElement.prototype);
SliderPrototype.createdCallback = function() {
    var self = this;
    this.sliderId = this.getAttribute("data-slider-id");
    
    var name = getName();

    window['sliderX' + name] = this;

    function getName(name){
        var name = self.getAttribute('data-slider-id');
        try {
            if(!name) throw new Error('Name for "ct-slider-x" must not be empty!');
            name = name.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
            name = name.replace(name[0], name[0].toUpperCase());
        } catch (e) {
          console.log(e.name + ': ' + e.message, self);
        }

        return name
    }



    var defWidthUnit = this.getAttribute('data-slide-width') || this.offsetWidth + 'px';
    this.slideWidth = defWidthUnit.match(/[a-zA-Z]+|[0-9]+/g)[0];
    this.slideWidthUnit = defWidthUnit.match(/[a-zA-Z]+|[0-9]+/g)[1];
    this.showSlidePerPage = this.getAttribute('data-show-slide') || 1;
    this.slideContainer = this.querySelector('slide-x-container');
    this.slideIndex = 0;
    this.prevSlideIndex = 0;
    this.length = this.querySelectorAll('slide-x').length;


    this.sliderIndicators = document.querySelector("slider-x-indicators[data-slider-id='" + self.sliderId + "']");
    
    this.refreshInterval = undefined;

    

    this.onSetSlide = function(){};



    function getOffSetLeft(){
        return 'calc(' + (- self.slideIndex * self.slideWidth) + self.slideWidthUnit + ')';
    }



    function nextIndex(isNext, notChangeIndex){
        var x = -1;
        if(isNext) x *= -1;
        self.prevSlideIndex = self.slideIndex;
        if(!notChangeIndex) self.slideIndex += x;

        var length = self.length;
        
        if(isNext  && self.slideIndex > length - self.showSlidePerPage) self.slideIndex = 0;  
        if(!isNext  && self.slideIndex < 0) self.slideIndex = length - self.showSlidePerPage; 
        
        if(self.sliderIndicators){
            self.sliderIndicators.querySelector("slider-x-indicator:nth-child(" + (self.prevSlideIndex + 1) + ")").classList.remove('active');
            self.sliderIndicators.querySelector("slider-x-indicator:nth-child(" + (self.slideIndex + 1) + ")").classList.add('active');
        }
        

        self.querySelector("slide-x:nth-child(" + (self.prevSlideIndex + 1) + ")").classList.remove('active');
        self.querySelector("slide-x:nth-child(" + (self.slideIndex + 1) + ")").classList.add('active');

        if(typeof self.onSetSlide == 'function') self.onSetSlide(self.getActiveSlide());

    }

    this.nextSlide = function(){
        nextIndex(true);
        self.slideContainer.style.left = getOffSetLeft();
    };

    this.prevSlide = function(){
        nextIndex();
        self.slideContainer.style.left = getOffSetLeft();
    };

    this.setSlide = function(index){
        self.prevSlideIndex = self.slideIndex;
        self.slideIndex = parseInt(index);

        if(self.sliderIndicators){
            self.sliderIndicators.querySelector("slider-x-indicator:nth-child(" + (self.prevSlideIndex + 1) + ")").classList.remove('active');
            self.sliderIndicators.querySelector("slider-x-indicator:nth-child(" + (self.slideIndex + 1) + ")").classList.add('active');
        }

        self.querySelector("slide-x:nth-child(" + (self.prevSlideIndex + 1) + ")").classList.remove('active');
        self.querySelector("slide-x:nth-child(" + (self.slideIndex + 1) + ")").classList.add('active');
        
        nextIndex(true, true);
        self.slideContainer.style.left = getOffSetLeft();
    };

    this.getActiveSlide = function(){
        return self.querySelector("slide-x:nth-child(" + (self.slideIndex + 1) + ")");
    };

    
    this.setInterval = function(time, reverse){
        if(self.refreshInterval) clearInterval(self.refreshInterval);
        self.refreshInterval = setInterval(reverse ? self.prevSlide : self.nextSlide, time);
    };

    this.stop = function(){
        if(self.refreshInterval) clearInterval(self.refreshInterval);
    };

    this.querySelector("slide-x:nth-child(" + ( this.slideIndex + 1 ) + ")").classList.add("active");

};
var Slider = document.registerElement('slider-x', {prototype: SliderPrototype});










var SlideContainerPrototype = Object.create(HTMLElement.prototype);
SlideContainerPrototype.createdCallback = function() { };
var SliderContainer = document.registerElement('slide-x-container', {prototype: SlideContainerPrototype});










var SlidePrototype = Object.create(HTMLElement.prototype);
SlidePrototype.createdCallback = function() { };
var Slide = document.registerElement('slide-x', {prototype: SlidePrototype});










var SliderControlPrototype = Object.create(HTMLElement.prototype);
SliderControlPrototype.createdCallback = function() {
    var self = this;
    this.action = this.getAttribute("data-slide");
    this.actionParams = this.getAttribute("data-params") ? '(' + this.getAttribute("data-params") + ')' : '()';
    slider = document.querySelector("slider-x[data-slider-id='" + this.getAttribute("data-slider-id") + "']");
    
    this.addEventListener("click", function(){
        eval('slider.' + self.action + self.actionParams);
    })
};
var SliderControl = document.registerElement('slider-x-control', {prototype: SliderControlPrototype});










var SliderIndicatorPrototype = Object.create(HTMLElement.prototype);
SliderIndicatorPrototype.createdCallback = function() {
    var self = this;
    
    

    this.addEventListener("click", function(){
        var sliderIndicators = this.parentNode;
        var nodeList = Array.prototype.slice.call( sliderIndicators.children );
        var currIndex = nodeList.indexOf(self);
        this.parentNode.slider.setSlide(currIndex);
    })

};
var SliderIndicator = document.registerElement('slider-x-indicator', {prototype: SliderIndicatorPrototype});










var SliderIndicatorsPrototype = Object.create(HTMLElement.prototype);
SliderIndicatorsPrototype.createdCallback = function() {
    var self = this;
    
    this.slider = document.querySelector("slider-x[data-slider-id='" + this.getAttribute("data-slider-id") + "']");
    
    this.sliderIndicator = document.createElement("slider-x-indicator");

    this.appendIndicator = function(items){
        self.appendChild(self.sliderIndicator.cloneNode());
        if(items) return self.appendIndicator(items - 1);
    };

    this.appendIndicator(self.slider.querySelectorAll('slide-x').length - 1);

    this.querySelector("slider-x-indicator:nth-child(" + ( this.slider.slideIndex + 1 ) + ")").classList.add("active");


};
var SliderIndicators = document.registerElement('slider-x-indicators', {prototype: SliderIndicatorsPrototype});
