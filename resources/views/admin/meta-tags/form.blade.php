<div class="form-group {{ $errors->has('page') ? 'has-error' : ''}}">
    {!! Form::label('page', 'Page', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('page', null, ['class' => 'form-control']) !!}
        {!! $errors->first('page', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('keywords') ? 'has-error' : ''}}">
    {!! Form::label('keywords', 'Keywords', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('keywords', null, ['class' => 'form-control']) !!}
        {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>