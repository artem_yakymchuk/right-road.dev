@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Metatags</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/meta-tags/create') }}" class="btn btn-primary btn-xs" title="Add New MetaTag"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th> Page </th><th> Keywords </th><th> Description </th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($metatags as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->page }}</td><td>{{ $item->keywords }}</td><td>{{ $item->description }}</td>
                                        <td>
                                            <a href="{{ url('/admin/meta-tags/' . $item->id) }}" class="btn btn-success btn-xs" title="View MetaTag"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/admin/meta-tags/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit MetaTag"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/meta-tags', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete MetaTag" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete MetaTag',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $metatags->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection