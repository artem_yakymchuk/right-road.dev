<header>
	<div class="logo"></div>

	<div class="line-menu">
		<div class="lm-background">
			<div class="lm-indicator"></div>
		</div>
	</div>
	<div class="m-line-menu">
		<div class="m-lm-indicator"></div>
	</div>
	
	<div class="m-menu-btn">
		<div></div>
		<div></div>
		<div></div>
	</div>
	<nav class="nav-menu">
		<div class="nm-item def-active">
			<div class="drop-down-menu">
				<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="0" class="ddm-header nm-item-main"> start </slider-x-control>
			</div>
		</div>
		
		<div class="nm-item open-drop-down-menu">
			<div class="drop-down-menu">
				<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="1" class="ddm-header nm-item-main">
					our services <span class="arrow"></span>
				</slider-x-control>
				<div class="ddm-content">
					<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="1" class="ddm-header">
						How we work
					</slider-x-control>
					<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="2" class="ddm-header">
						Services
					</slider-x-control>
					<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="3" class="ddm-header">
						Technology
					</slider-x-control>
				</div>
			</div>
		</div>

		<div class="nm-item open-drop-down-menu">
			<div class="drop-down-menu">
				<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="4" class="ddm-header nm-item-main">
					about us <span class="arrow"></span>
				</slider-x-control>
				<div class="ddm-content">
					<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="4" class="ddm-header">
						Our team
					</slider-x-control>
					<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="5" class="ddm-header">
						Blog
					</slider-x-control>
				</div>
			</div>
		</div>
		
		<div class="nm-item open-drop-down-menu">
			<div class="drop-down-menu">
				<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="6" class="ddm-header nm-item-main">
					contacts <span class="arrow"></span>
				</slider-x-control>
				<div class="ddm-content">
					<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="6" class="ddm-header">
						Ukraine
					</slider-x-control>
					<slider-x-control data-slider-id="main" data-slide="setSlide" data-params="7" class="ddm-header">
						Poland
					</slider-x-control>
				</div>
			</div>
		</div>
	</nav>
</header>