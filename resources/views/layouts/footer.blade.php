<footer>
	<div class="footer-icons">
		<a href="https://www.facebook.com/Right-Road-1419350351668912/" class="hexagon-filled-h">
			<div class="hexagon-con full-size flex ai-center jc-center">
				<div class="social-icon facebook-icon"></div>
			</div>
		</a>

		<a href="https://vk.com/rightroad" class="hexagon-filled-h">
			<div class="hexagon-con full-size flex ai-center jc-center">
				<div class="social-icon vk-icon"></div>
			</div>
		</a>

		<a href="https://www.linkedin.com/company-beta/17903877/" class="hexagon-filled-h">
			<div class="hexagon-con full-size flex ai-center jc-center">
				<div class="social-icon linkedin-icon"></div>
			</div>
		</a>
	</div>
</footer> 