<div class="mc-item">
	
	<div class="mc-content flex ai-center m-column-reverse m-padding-b-50">
		<div class="flex-3">
			<h1> our<br> technologies </h1>
			<p>
				For our projects we use latest technologies, such as PHP, HTML5, AngularJS, Python, Laravel, django, JavaScript, jquery, ExpressJS, CSS3, Java, Microsoft.
			</p>
			<p>
				Our designers create eye-catching visual content, using Adobe Photoshop and Adobe Illustrator.
			</p>
			<p>
				We also develop mobile apps for Android, IOS and Windows Mobile.
			</p>
		</div>
		<div class="flex-8 full-size our-tecnology-img"></div>
	</div>

	<slider-x-control data-slider-id="main" data-slide="nextSlide" data-menu-index="2" class="mc-btn-next hexagon-b-filled-h">
		<span class="arrow-right"></span>
	</slider-x-control>

</div>