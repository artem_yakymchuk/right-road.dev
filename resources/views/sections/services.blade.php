<div class="mc-item our-services">
	<div class="mc-image"></div>
	
	<div class="mc-content">
		
		<div class="title m-padding-b-50">
			<h1>our <br> services</h1>
		</div>

		<div class="hexagon-container services">
			<div class="row">
				<div class="hexagon-lg gray-1">
					@include('svg.hexagon-filled')
					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							We offer a full range of custom software development services, whether for a big company or startup, to assist you at any stage of development process. 
						</div>
					</div>
					<div class="hexagon-label"> software development </div>
				</div>	
			</div>


			<div class="row">
				<div class="hexagon-lg gray-3">
					@include('svg.hexagon-filled')
					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							Our IT consultants can help your organization to assess its use of technology and to define and deliver an IT strategy.
						</div>
					</div>
					<div class="hexagon-label"> IT business Consulting </div>
				</div>
			</div>



			<div class="row">
				<div class="hexagon-lg gray-4 white-label">
					@include('svg.hexagon-filled')
					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							Our professional QA Engineers carry out software testing services on each stage of software development. We also offer advanced testing, depending on your needs.
						</div>
					</div>	
					<div class="hexagon-label"> software testing / qa </div>
				</div>
			</div>	

			
			<div class="row desctop-v">
				<div class="hexagon-lg transparent"></div>
			</div>


			<div class="row">
				<div class="hexagon-lg black white-label">
					@include('svg.hexagon-filled')
					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							There are only experts in our team, who successfully delivered a wide range of projects. We can allocate a whole team for you to support you during all stages of development.
						</div>
					</div>
					<div class="hexagon-label"> development team </div>
				</div>
			</div>	
			

			<div class="row">
				<div class="hexagon-lg yellow">
					@include('svg.hexagon-filled')
					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							Outsource your IT functions to our team and enjoy the benefits of working with professionals.
						</div>
					</div>
					<div class="hexagon-label"> IT business process outsourcing</div>
				</div>
			</div>	
		</div>

		<div class="mob-pop-up">
			<div class="m-pp-content">
				
			</div>
		</div>

	</div>

	<slider-x-control data-slider-id="main" data-slide="nextSlide" data-menu-index="1" class="mc-btn-next hexagon-b-filled-h">
		<span class="arrow-right"></span>
	</slider-x-control>

</div>