<div class="mc-item reverse-color">
	
	<div class="mc-image" style="background-color: black"></div>
	
	<div class="mc-content flex ai-center m-ai-bottom m-padding-b-50">
		<div class="flex-2 desctop-v"></div>
		<div class="flex-3 right-t">
			<h1> about our<br> team </h1>
			<p>
				Right-road is a team of experts who are passionate about IT. 
			</p>
			<p>
				Our staff includes back-end and front-end developers, designers, QA engineers and consultants. 
			</p>
		</div>
		<div class="flex-4 desctop-v"></div>
	</div>

	<slider-x-control data-slider-id="main" data-slide="nextSlide" data-menu-index="2" class="mc-btn-next hexagon-b-filled-h">
		<span class="arrow-right"></span>
	</slider-x-control>

</div>