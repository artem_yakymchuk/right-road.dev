<div class="mc-item our-services">
    <div class="mc-image">
        <div class="mc-contacts">
            Poland
        </div>
    </div>

    <div class="mc-content flex ai-center">
        <div class="flex-2 desctop-v"></div>
        <div class="flex-6 relative">
            <div class="pop-up pop-up-contact">
                <div class="pop-up-open"></div>
                <div class="pop-up-content">
                    <form is="ct-form">
                        <div class="form-group">
                            <input class="pc-input" type="text" name="name" placeholder="Your email">
                        </div>
                        <div class="form-group">
                            <textarea class="pc-input" rows="1" placeholder="Your message"></textarea>
                        </div>
                        <div class="container-pc-btn">
                            <input type="button" class="pc-btn" value="Send">
                        </div>
                    </form>
                </div>
            </div>
          
            <h2>Partner</h2>
            <div class="flex">
                <div class="flex-3">
                    <div class="flex m-column">
                        <div class="flex-1">
                            <span class="phone-number">+ 48 735 460 115</span>
                        </div>
                        <div class="flex-1 right">
                            <a href="http://ridanta.com" class="web-site">ridanta.com</a>
                        </div>
                    </div>
                </div>
                <div class="flex-2"></div>
            </div>
            
            <p class="email"> <span>support</span><span class="dog">@</span><span>ridanta.com</span> </p>
            <div class="address">
                <div>Florianska str., №6, office 02,</div>
                <div>Warsaw, Poland</div>
            </div>

            <div class="map" id="map-pl"></div>
        </div>
        <div class="flex-2 desctop-v"></div>
    </div>
    <div></div>
</div>