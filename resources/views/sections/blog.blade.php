<div class="mc-item our-services">
	<div class="mc-image mc-image-fixed" style="background-image: url({{ url('/img/blog.jpg') }});"></div>
	
	<div class="mc-content mc-blog ov-hidden">
		
		<div class="title">
			<h1>blog</h1>
		</div>

		<div class="hexagon-container blog">
			<div class="item">
				<div class="hexagon-lg with-img"></div>	

				<div class="hexagon-lg transparent gray-1-h bold">
					@include('svg.hexagon-filled')

					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							<p>
								A software developer is a person concerned with facets of the software development process, including the research, design, programming, and testing of computer software. Other job titles which are often used with similar meanings are programmer, software analyst, and software engineer. According to developer Eric Sink, the differences between system design, software development, and programming are more apparent. Already in the current market place there can be found a segregation between programmers and developers,  being that one who implements is not the same as the one who designs the class structure or hierarchy. Even more so that developers become systems architects, those who design the multi-leveled architecture or component interactions of a large software system.
							</p>
							<p>
								In a large company, there may be employees whose sole responsibility consists of only one of the phases above. In smaller development environments, a few people or even a single individual might handle the complete process.
							</p>
						</div>
					</div>
					<div class="hexagon-label"> software developer </div>
					<div class="date">19 April 2016</div>
				</div>

				<div class="hexagon-lg black yellow-h white-label">
					@include('svg.hexagon-filled')
					<div class="text">
						A software developer is a person concerned with facets of the software development process, including ...
					</div>
				</div>	
			</div>



			<div class="item">
				<div class="hexagon-lg black yellow-h white-label">
					@include('svg.hexagon-filled')
					<div class="text">
						Software testing is an investigation conducted to provide stakeholders with information about the quality of the ...
					</div>
				</div>	

				<div class="hexagon-lg with-img"></div>	
				
				<div class="hexagon-lg transparent gray-1-h bold">
					@include('svg.hexagon-filled')

					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							<p>
								Software testing is an investigation conducted to provide stakeholders with information about the quality of the product or service under test. Software testing can also provide an objective, independent view of the software to allow the business to appreciate and understand the risks of software implementation. Test techniques include the process of executing a program or application with the intent of finding software bugs (errors or other defects), and to verify that the software product is fit for use.
							</p>
							<p>
								Software testing involves the execution of a software component or system component to evaluate one or more properties of interest. In general, these properties indicate the extent to which the component or system under test:
							</p>
							<ul>
								<li>
									meets the requirements that guided its design and development,
								</li>
								<li>
									responds correctly to all kinds of inputs,
								</li>
								<li>
									performs its functions within an acceptable time,
								</li>
								<li>
									is sufficiently usable,
								</li>
								<li>
									can be installed and run in its intended environments, and
								</li>
								<li>
									achieves the general result its stakeholders desire.
								</li>
							</ul>
						</div>
					</div>
					<div class="hexagon-label"> software testing / qa </div>
					<div class="date">19 April 2016</div>
				</div>

			</div>



			<div class="item">
				<div class="hexagon-lg with-img"></div>	

				<div class="hexagon-lg transparent gray-1-h bold">
					@include('svg.hexagon-filled')

					<div class="pop-up">
						<div class="pop-up-open"></div>
						<div class="pop-up-content">
							<p>
								Right-road is a team of experts who are passionate about IT. Our staff includes back-end and front-end developers, designers, QA engineers and consultants.
							</p>
							<p>
								Successful projects are usually the result of careful planning and the talent and collaboration of a project's team members. Project can't move forward without each of its key team members. 
							</p>
						</div>
					</div>
					<div class="hexagon-label"> development team </div>
					<div class="date">19 April 2016</div>
				</div>

				<div class="hexagon-lg black yellow-h white-label">
					@include('svg.hexagon-filled')
					<div class="text">
						Right-road is a team of experts who are passionate about IT. Our staff includes back-end and front-end developers ...
					</div>
				</div>	
			</div>
		</div>

		<div class="mob-pop-up">
			<div class="m-pp-content">
				<div class="m-pp-c-header"></div>
				<div class="m-pp-c-date"></div>
				<div class="m-pp-c-content"></div>
			</div>
		</div>
		
	</div>

	<slider-x-control data-slider-id="main" data-slide="nextSlide" data-menu-index="3" class="mc-btn-next hexagon-b-filled-h">
		<span class="arrow-right"></span>
	</slider-x-control>

</div>