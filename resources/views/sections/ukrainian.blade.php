<div class="mc-item our-services">
    <div class="mc-image">
        <div class="mc-contacts">
            Ukrainian
        </div>
    </div>

    <div class="mc-content flex ai-center">
        <div class="flex-2 desctop-v"></div>
        <div class="flex-6 relative">
            <div class="pop-up pop-up-contact">
                <div class="pop-up-open"></div>
                <div class="pop-up-content">
                    <form is="ct-form">
                        <div class="form-group">
                            <input class="pc-input" type="text" name="name" placeholder="Your email">
                        </div>
                        <div class="form-group">
                            <textarea class="pc-input" rows="1" placeholder="Your message"></textarea>
                        </div>
                        <div class="container-pc-btn">
                            <input type="button" class="pc-btn" value="Send">
                        </div>
                    </form>
                </div>
            </div>
          
            <h2>Main office</h2>
            <span class="phone-number">+38 050 100 0388</span>
            <p class="email"> <span>support</span><span class="dog">@</span><span>right-road.com</span> </p>
            <div class="address">
                <div>Holovna st. 204B</div>
                <div>Chernivtsi, Ukraine</div>
            </div>

            <div class="map" id="map-ua"></div>
        </div>
        <div class="flex-2 desctop-v"></div>
    </div>

    <slider-x-control data-slider-id="main" data-slide="nextSlide" data-menu-index="3" class="mc-btn-next hexagon-b-filled-h">
        <span class="arrow-right"></span>
    </slider-x-control>

</div>