<div class="mc-item">
	<div class="mc-image start-image"></div>
	
	<div class="mc-content flex ai-center m-ai-bottom m-padding-b-50">
		<div class="flex-4">
			<h1> the new perspective </h1>
			<p>
				Right-road was founded in 2012 and since then we have strived to deliver result, which will exceed our client’s expectations. Our main goal is not only to complete the project, but also to be sure, that it will be successful.
			</p>
		</div>
		<div class="flex-9 m-flex-2"></div>
	</div>

	<slider-x-control data-slider-id="main" data-slide="nextSlide" data-menu-index="1" class="mc-btn-next hexagon-b-filled-h">
		<span class="arrow-right"></span>
	</slider-x-control>

</div>