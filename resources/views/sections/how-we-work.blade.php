<div class="mc-item reverse-color">
	
	<div class="mc-image" style="background-color: black"></div>
	
	<div class="mc-content flex ai-center m-ai-bottom">
		<div class="flex-2 desctop-v"></div>
		<div class="flex-3 right-t m-padding-b-50">
			<h1> We command<br> Dreams </h1>
			<p>
				Here is a little insight into how we turn your dreams into reality. We offer several services, so feel free to choose the one, which suits best your business needs. We can provide you estimated price, based on the complexity and the amount of time that would be needed to complete the project. We can charge you on an hourly basis. We can provide you our team of professionals for constant support of your project.
			</p>
		</div>
		<div class="flex-4 desctop-v"></div>
	</div>

	<slider-x-control data-slider-id="main" data-slide="nextSlide" data-menu-index="1" class="mc-btn-next hexagon-b-filled-h">
		<span class="arrow-right"></span>
	</slider-x-control>

</div>