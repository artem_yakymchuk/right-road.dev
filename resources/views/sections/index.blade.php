<!doctype html>
<html lang="en">
<head>

    <title>Right Road</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=768">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">

    @section('stylesheet')
        <link rel="stylesheet" type="text/css" href="{{url('/css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/app-reverse-color.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/base64.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/ct-slider-x.css')}}">

        <link rel="stylesheet" type="text/css" href="{{url('/css/mobile-app.css')}}">
    @show

</head>
<body>



    @include('layouts.header')



    <slider-x class="mc" data-slider-id="main" data-slide-width="100vw" data-show-slide="1">
        <slide-x-container>
            <slide-x> 
                @include('sections.start') </slide-x>
            <slide-x data-reverse-color> 
                @include('sections.how-we-work') </slide-x>
            <slide-x> 
                @include('sections.services') </slide-x>
            <slide-x> 
                @include('sections.technology') </slide-x>
            <slide-x data-reverse-color>  
                @include('sections.our-team') </slide-x>
            <slide-x> 
                @include('sections.blog') </slide-x>
            <slide-x> 
                @include('sections.ukrainian') </slide-x>
            <slide-x> 
                @include('sections.poland') </slide-x>
            
        </slide-x-container>    
    </slider-x>



    @include('layouts.footer')
	


    @section('scripts')
        <script>

            function supportsCustomElements() {
                return 'registerElement' in document;
            };

            if (!supportsCustomElements()) {
                document.write("<script src='{{url('/js/webcomponents-lite.js')}}'>\<\/script>");
            };

        </script>

        <script type="text/javascript" src="{{url('/js/hummer.min.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/ct-slider-x.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/app.js')}}"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFQedlMwO_fPg0ybcg1i9YisEQYAfgHa8&callback=initMap"></script>

    @show
    
</body>
</html>