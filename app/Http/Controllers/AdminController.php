<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function getFeedback(){
        $feedback = Feedback::all();
        return view('admin.feedback',compact('feedback'));
    }
}
