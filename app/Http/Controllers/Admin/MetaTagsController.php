<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MetaTag;
use Illuminate\Http\Request;
use Session;

class MetaTagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $metatags = MetaTag::paginate(25);

        return view('admin.meta-tags.index', compact('metatags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.meta-tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        MetaTag::create($requestData);

        Session::flash('flash_message', 'MetaTag added!');

        return redirect('admin/meta-tags');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $metatag = MetaTag::findOrFail($id);

        return view('admin.meta-tags.show', compact('metatag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $metatag = MetaTag::findOrFail($id);

        return view('admin.meta-tags.edit', compact('metatag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $metatag = MetaTag::findOrFail($id);
        $metatag->update($requestData);

        Session::flash('flash_message', 'MetaTag updated!');

        return redirect('admin/meta-tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        MetaTag::destroy($id);

        Session::flash('flash_message', 'MetaTag deleted!');

        return redirect('admin/meta-tags');
    }
}
